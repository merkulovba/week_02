package ru.edu.empty;

import ru.edu.SourceReader;
import ru.edu.TextAnalyzer;
import ru.edu.TextStatistics;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Пустая реализация для компиляции
 */
public class EmptySourceReader implements SourceReader {

    @Override
    public void setup(String source) {
        File file = new File(source);
        assertTrue(System.getProperty("user.dir"), file.exists());
    }

    @Override
    public TextStatistics readSource(TextAnalyzer analyzer) {
        return new TextStatistics();
    }
}
